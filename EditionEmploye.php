<?php

session_start();
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>edition salarie</title>
</head>

<body>
    <?php
    include "config.php";
    include "nav.html";
    ?>

    <h2>EDITER UN SALARIÉ</h2>


    <?php
    $id_salarie = $_GET['id_salarie'];

    try {
        $requete = $pdo->prepare("SELECT * FROM salarie WHERE id_salarie=?");
        $requete->execute([$id_salarie]);
        $requete = $requete->fetchAll();
        $salarie = $requete[0];
    } catch (PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }

    $nom           = isset($_POST['nom'])           && !empty($_POST['nom'])           ? $_POST['nom']           : '';
    $prenom        = isset($_POST['prenom'])        && !empty($_POST['prenom'])        ? $_POST['prenom']        : '';
    $adresse       = isset($_POST['adresse'])       && !empty($_POST['adresse'])       ? $_POST['adresse']       : '';
    $code_postal   = isset($_POST['code_postal'])   && !empty($_POST['code_postal'])   ? $_POST['code_postal']   : '';
    $ville         = isset($_POST['ville'])         && !empty($_POST['ville'])         ? $_POST['ville']         : '';
    $telephone     = isset($_POST['telephone'])     && !empty($_POST['telephone'])     ? $_POST['telephone']     : '';
    $date_embauche = isset($_POST['date_embauche']) && !empty($_POST['date_embauche']) ? $_POST['date_embauche'] : '';
    $identifiant   = isset($_POST['identifiant'])   && !empty($_POST['identifiant'])   ? $_POST['identifiant']   : '';
    $mdp           = isset($_POST['mot_de_passe'])  && !empty($_POST['mot_de_passe'])  ? $_POST['mot_de_passe']  : '';
    $salarie_role  = isset($_POST['salarie_role'])  && !empty($_POST['salarie_role'])  ? $_POST['salarie_role'] : '';
    $submit        = isset($_POST['submit'])        && !empty($_POST['submit'])        ? $_POST['submit']        : '';

    if ($submit) {

        try {

            $request = $pdo->prepare("UPDATE salarie SET nom=:nom,prenom=:prenom,adresse=:adresse,code_postal=:code_postal,ville=:ville,
                    telephone=:telephone,date_embauche=:date_embauche,identifiant=:identifiant,mdp=:mdp,salarie_role=:salarie_role
                    WHERE id_salarie=$id_salarie");

            $request->execute([
                'nom' => $nom, 'prenom' => $prenom, 'adresse' => $adresse, 'code_postal' => $code_postal, 'ville' => $ville, 'telephone' => $telephone,
                'date_embauche' => $date_embauche, 'identifiant' => $identifiant, 'mdp' => $mdp, 'salarie_role' => $salarie_role
            ]);
            header('Location: listeEmploye.php');
        } catch (PDOException $e) {
            echo 'Error ' . $e->getMessage();
        }
    }

    /* SUPPRESSION D'UN EMPLOYE */
    $sup = isset($_GET['sup'])   && !empty($_GET['sup'])   ? $_GET['sup']   : '';
    $id  = isset($_GET['id'])    && !empty($_GET['id'])    ? $_GET['id']    : '';

    if ($sup == 'sup') {
        /* if ($id != null) { */
        $req = $pdo->prepare("DELETE FROM salarie WHERE id_salarie=?");
        $req->execute([$id]);
        echo 'supprimé';
        /* header('Location:employe.php'); */
        /*}*/
    }

    ?>

    <form method="post">
        <div class="container-fluid text-center">
            <div class="row m-5">
                <div class="col-4 bg-dark text-white py-3">
                    <h2><?php echo $salarie['prenom'] . ' ' . $salarie['nom']; ?></h2>
                </div>
                <div class="col-1 mt-3 px-4">
                    <a href="InfosEmploy.php" class="btn btn-dark text-danger p-2">Retour</a>
                </div>
            </div>
        </div>

        <div class="container mt-5 d-flex font-weight-bold text-center border-bottom border-danger">
            <div class="container">

                <div class="col-10">
                    Nom<br><input type="text" name="nom" value="<?php echo $salarie['nom']; ?>">
                </div>
                <div class="col-10">
                    Prénom<br><input type="text" name="prenom" value="<?php echo $salarie['prenom']; ?>">
                </div>
                <div class="col-10">
                    Adresse<br><input type="text" name="adresse" value="<?php echo $salarie['adresse']; ?>">
                </div>
                <div class="col-10">
                    Code postal<br><input type="text" name="code_postal" value="<?php echo $salarie['code_postal']; ?>">
                </div>
                <div class="col-10">
                    Ville<br><input type="text" name="ville" value="<?php echo $salarie['ville']; ?>">
                </div>
            </div>

            <div class="container">
                <div class="col-10">
                    Téléphone<br><input type="text" name="telephone" value="<?php echo $salarie['telephone']; ?>">
                </div>
                <div class="col-10">
                    Date d'embauche <input type="text" name="date_embauche" value="<?php echo $salarie['date_embauche']; ?>">
                </div>
                <div class="col-10">
                    Identifiant <input type="text" name="identifiant" value="<?php echo $salarie['identifiant']; ?>">
                </div>
                <div class="col-10">
                    Mot de passe <input type="password" name="mot_de_passe" value="<?php echo $salarie['mot_de_passe']; ?>">
                </div>
                <div class="col-10">
                    Role du salarie <input type="text" name="salarie_role" value="<?php echo $salarie['salarie_role']; ?>">
                </div>
            </div>
        </div>
        <div class="container border-bottom border-danger">
            <div class="col-4 my-3 offset-4 text-center">
                <input class="mb-0" type="submit" name="submit" value="Enregistrer">
            </div>
        </div>
    </form>



</body>

</html>