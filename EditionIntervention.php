<?php

session_start();

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Edition Interventions</title>
</head>

<body>

    <?php

    include "config.php";

    include "nav.html";

    $recupIntitule = isset($_POST['intitule']) && !empty($_POST['intitule']) ? $_POST['intitule'] : "";
    $recupDate = isset($_POST['date']) && !empty($_POST['date']) ? $_POST['date'] : "";
    $recupHeure = isset($_POST['heure']) && !empty($_POST['heure']) ? $_POST['heure'] : "";
    $recupDuree = isset($_POST['duree']) && !empty($_POST['duree']) ? $_POST['duree'] : "";
    $recupClient = isset($_POST['client']) && !empty($_POST['client']) ? $_POST['client'] : "";
    $recupDescription = isset($_POST['description']) && !empty($_POST['description']) ? $_POST['description'] : "";
    $recupEmploye = isset($_POST['employe']) && !empty($_POST['employe']) ? $_POST['employe'] : "";

    $recupIdIntervention = isset($_GET["id_intervention"]) ? $_GET["id_intervention"] : "";
    $recupIdSalarie = isset($_GET["id_salarie"]) ? $_GET["id_salarie"] : "";

    try {
        $req = $pdo->prepare("SELECT * FROM intervention_salarie
                         INNER JOIN salarie ON salarie.id_salarie = intervention_salarie.id_salarie
                         INNER JOIN intervention ON intervention.id_intervention = intervention_salarie.id_intervention
                         WHERE intervention.id_intervention = ? AND salarie.id_salarie = ?
                         ");
        $req->execute([$recupIdIntervention, $recupIdSalarie]);
        $results = $req->fetchALL();
        $stockInfos = $results[0];
    } catch (PDOException $e) {
        echo "Erreur select: " . $e->getMessage();
    }

    ?>

    <div id="formulaireAjoutIntervention">

        <h1> Fiche d'intervention n°<?php echo $stockInfos['id_intervention']; ?> </h1>

        <form id="form" method="post">
            <div class="div1">
                <div>
                    <p>Intitulé</p> <input type="text" name="intitule" placeholder="" value="<?php echo $stockInfos['intitule'] ?>">
                </div>
                <div>
                    <p>Date</p><input type="date" name="date" placeholder="" value="<?php echo $stockInfos['date_inter'] ?>">
                </div>
            </div>
            <div class="div1">
                <div>
                    <p>Heure</p> <input type="text" name="heure" placeholder="" value="<?php echo $stockInfos['heure_inter'] ?>">
                </div>
                <div>
                    <p>Durée (en min)</p> <input type="number" name="duree" placeholder="" value="<?php echo $stockInfos['duree'] ?>">
                </div>
            </div>
            <div id="div2">
                <div>
                    <p>Client (Nom Prénom)</p> <input type="text" name="client" placeholder="" value="<?php echo $stockInfos['nom_prenom_client'] ?>">
                </div>
            </div>
            <div id="div3">
                <p>Description</p><textarea name="description" placeholder="" rows="5" cols="33"><?php echo $stockInfos['description_inter'] ?></textarea>
            </div>
            <div id="div4">
                <div>
                    <p>Intervenant(s)</p><select name="employe" multiple size=5>
                        <?php

                        try {
                            $req = $pdo->prepare("SELECT nom, prenom, id_salarie FROM salarie");
                            $req->execute();
                            $results = $req->fetchAll();
                            foreach ($results as $salarie) {
                                echo "<option value='" . $salarie['id_salarie'] . "'>" . $salarie['nom'] . " " . $salarie['prenom'] . "</option>";
                            }
                        } catch (PDOException $e) {
                            echo "Erreur select : " . $e->getMessage();
                        }

                        ?>
                    </select>
                </div>
                <div><input class="submit" type="submit" name="submit" id="boutonAjout" value="Modifier"></div>
            </div>
        </form>

    </div>

    <?php

    if (isset($_POST['submit'])) {
        if (
            isset($_POST['intitule']) && !empty($_POST['intitule'])
            && isset($_POST['date']) && !empty($_POST['date'])
            && isset($_POST['duree']) && !empty($_POST['duree'])
            && isset($_POST['client']) && !empty($_POST['client'])
            && isset($_POST['description']) && !empty($_POST['description'])
        ) {
            try {
                $req = $pdo->prepare("UPDATE intervention SET nom_prenom_client=?, date_inter=?, heure_inter=?, intitule=?, description_inter=?, duree=? WHERE id_intervention=$recupIdIntervention");
                $req->execute([$recupClient, $recupDate, $recupHeure, $recupIntitule, $recupDescription, $recupDuree]);
            } catch (PDOException $e) {
                echo "Erreur update infos intervention: " . $e->getMessage();
            }

            try {
                $req = $pdo->prepare("UPDATE intervention_salarie SET id_salarie=? WHERE id_intervention=$recupIdIntervention");
                $req->execute([$recupEmploye]);
                header("Location: listeInterventions.php");
            } catch (PDOException $e) {
                echo "Erreur update infos intervention: " . $e->getMessage();
            }
        } else {
            echo '<p>Veuillez remplir tous les champs SVP</p>';
        }
    }

    ?>




</body>

</html>