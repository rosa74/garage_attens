<?php

session_start();
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Infos employé</title>
</head>

<body>
    <?php
    include "config.php";
    include "navAdmin.html";


    $recupIdSalarie = isset($_GET["id_salarie"]) ? $_GET["id_salarie"] : "";

    try {
        $req = $pdo->prepare("SELECT * FROM salarie WHERE id_salarie = :id");;
        $req->execute(["id" => $recupIdSalarie]);
        $stockInfos = $req->fetch();
        // $stockInfos = $results[0];
    } catch (PDOException $e) {
        echo "Erreur select: " . $e->getMessage();
    }
    $supSalarie = isset($_GET["sup"]) ? $_GET["sup"] : "";

    if ($supSalarie == 'ok') {
        try {
            $req = $pdo->prepare("DELETE FROM salarie WHERE id_salarie = id ");
            $req->execute(["id" => $recupIdSalarie]);
        } catch (PDOException $e) {
            echo "Erreur suppression: " . $e->getMessage();
        }

        try {
            $req = $pdo->prepare("DELETE FROM salarie WHERE id_salarie = id");
            $req->execute(["id" => $recupIdSalarie]);
            header("Location: listeEmploye.php");
        } catch (PDOException $e) {
            echo "Erreur suppression: " . $e->getMessage();
        }
    }

    ?>

    <div id="Infos_Employe">

        <div id="formulaire">
            <div class="div1">
                <div>
                    <p class="nomEmploye">NOM</p>
                    <p class="infos"><?php echo $stockInfos['nom']; ?></p>
                </div>
                <div>
                    <p class="nomEmploye">Prenom</p>
                    <p class="infos"><?php echo $stockInfos['prenom']; ?></p>
                </div>
            </div>
            <div class="div1">
                <div>
                    <p class="nomEmploye">Adresse</p>
                    <p class="infos"><?php echo $stockInfos['adresse']; ?></p>
                </div>
                <div>
                    <p class="nomEmploye">Code_postal</p>
                    <p class="infos"><?php echo $stockInfos['code_postal']; ?></p>
                </div>
            </div>
            <div id="div2">
                <div>
                    <p class="nomEmploye">Ville</p>
                    <p class="infos"><?php echo $stockInfos['ville']; ?></p>
                </div>
            </div>
            <div id="div3">
                <p class="nomEmploye">Telephone</p>
                <p class="infos"><?php echo $stockInfos['telephone']; ?></p>
            </div>
            <div id="div4">
                <div>
                    <p class="nomEmploye">Mail</p>
                    <p class="infos"><?php echo $stockInfos['mail']; ?></p>
                    <p class="nomEmploye">Identifiant</p>
                    <p class="infos"><?php echo $stockInfos['identifiant']; ?></p>
                    <p class="nomEmploye">Mdp</p>
                    <p class="infos"><?php echo $stockInfos['mdp']; ?></p>
                    <p class="nomEmploye">Role du salarie</p>
                    <p class="infos"><?php echo $stockInfos['salarie_role']; ?></p>
                </div>
            </div>
        </div>

    </div>

    <div id="troisBoutons">
        <div>
            <?php echo "<a href='EditionEmploye.php?id_salarie=" . ($stockInfos['id_salarie']) . "&id_salarie=" . ($stockInfos['id_salarie']) . "'><p>Editer</p></a>"; ?>
        </div>
        <div>
            <?php echo "<a href='?sup=ok&id_intervention=" . ($stockInfos['id_salarie']) . "&id_salarie=" . ($stockInfos['id_salarie']) . "'><p>Supprimer</p></a>"; ?>
        </div>
        <div>
            <a href="listeEmploye.php">

                <p>Retour à la liste</p>
            </a>
        </div>
    </div>
    <div id="divListeInterventions">
        <p>Liste des interventions programmées (par ordre chronologique) :<p>
    </div>

    <?php

    try {
        $req = $pdo->prepare("SELECT * FROM intervention_salarie
                         INNER JOIN salarie ON salarie.id_salarie = intervention_salarie.id_salarie
                         INNER JOIN intervention ON intervention.id_intervention = intervention_salarie.id_intervention
                         WHERE DATE(intervention.date_inter) > '2020-03-20' AND (salarie.id_salarie) = ?
                         ORDER BY intervention.date_inter DESC, intervention.heure_inter DESC
                         ");
        $req->execute([$recupIdSalarie]);
        $results = $req->fetchALL();
        echo '<table><tr><th>Date</th><th>Heure</th><th>Intitulé</th><th>Client</th><th>Employé(s)</th><th></th></tr>';
        foreach ($results as $intervention) {
            echo "  <tr>
                    <td>" . ($intervention['date_inter']) . "</td>
                    <td>" . ($intervention['heure_inter']) . "</td>
                    <td>" . ($intervention['intitule']) . "</td>
                    <td>" . ($intervention['nom_prenom_client']) . "</td>
                    <td>" . ($intervention['nom'] . " " . $intervention['prenom']) . "</td>
                    <td><div id='boutonVoir'>
                        <a href='InfosIntervention.php?id_intervention=" . ($intervention['id_intervention']) . "&id_salarie=" . ($intervention['id_salarie']) . "'>Voir</a>
                        </div></td>
                </tr>";
        }
        echo '</table>';
    } catch (PDOException $e) {
        echo "Erreur affichage liste: " . $e->getMessage();
    }

    ?>

    <div id="divListeInterventions">
        <p>Liste des interventions effectuées (par ordre chronologique) :<p>
    </div>

    <?php

    try {
        $req = $pdo->prepare("SELECT * FROM intervention_salarie
                         INNER JOIN salarie ON salarie.id_salarie = intervention_salarie.id_salarie
                         INNER JOIN intervention ON intervention.id_intervention = intervention_salarie.id_intervention
                         WHERE DATE(intervention.date_inter) <= '2020-03-20' AND (salarie.id_salarie) = ?
                         ORDER BY intervention.date_inter DESC, intervention.heure_inter DESC
                         ");
        $req->execute([$recupIdSalarie]);
        $results = $req->fetchALL();
        echo '<table><tr><th>Date</th><th>Heure</th><th>Intitulé</th><th>Client</th><th>Employé(s)</th><th></th></tr>';
        foreach ($results as $intervention) {
            echo "  <tr>
                    <td>" . ($intervention['date_inter']) . "</td>
                    <td>" . ($intervention['heure_inter']) . "</td>
                    <td>" . ($intervention['intitule']) . "</td>
                    <td>" . ($intervention['nom_prenom_client']) . "</td>
                    <td>" . ($intervention['nom'] . " " . $intervention['prenom']) . "</td>
                    <td><div id='boutonVoir'>
                        <a href='InfosIntervention.php?id_intervention=" . ($intervention['id_intervention']) . "&id_salarie=" . ($intervention['id_salarie']) . "'>Voir</a>
                        </div></td>
                </tr>";
        }
        echo '</table>';
    } catch (PDOException $e) {
        echo "Erreur affichage liste: " . $e->getMessage();
    }

    ?>

</body>

</html>