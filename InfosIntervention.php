<?php

session_start();

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>infos interventions</title>
</head>

<body>

    <?php
    include "config.php";
    include "nav.html";

    $recupIdIntervention = isset($_GET["id_intervention"])?$_GET["id_intervention"] : "";
    $recupIdSalarie = isset($_GET["id_salarie"])?$_GET["id_salarie"] : "";

    try {
    $req = $pdo->prepare("SELECT * FROM intervention_salarie
    INNER JOIN salarie ON salarie.id_salarie = intervention_salarie.id_salarie
    INNER JOIN intervention ON intervention.id_intervention = intervention_salarie.id_intervention
    WHERE intervention.id_intervention = ? AND salarie.id_salarie = ?
    ");
    $req->execute([$recupIdIntervention, $recupIdSalarie]);
    $results = $req->fetchALL();
    $stockInfos = $results[""];
    }

    catch(PDOException $e) {
    echo "Erreur select: ".$e->getMessage();
    }

    ?>

    <div id="Infos_Intervention">

        <h1> Fiche d'intervention n°<?php echo $stockInfos['id_intervention']; ?> </h1>

        <div id="form">
            <div class="div1">
                <div>
                    <p class="nomInfos">Intitulé</p>
                    <p class="infos"><?php echo $stockInfos['intitule']; ?></p>
                </div>
                <div>
                    <p class="nomInfos">Date</p>
                    <p class="infos"><?php echo $stockInfos['date_inter']; ?></p>
                </div>
            </div>
            <div class="div1">
                <div>
                    <p class="nomInfos">Heure</p>
                    <p class="infos"><?php echo $stockInfos['heure_inter']; ?></p>
                </div>
                <div>
                    <p class="nomInfos">Durée (en min)</p>
                    <p class="infos"><?php echo $stockInfos['duree']; ?></p>
                </div>
            </div>
            <div id="div2">
                <div>
                    <p class="nomInfos">Client (Nom Prénom)</p>
                    <p class="infos"><?php echo $stockInfos['nom_prenom_client']; ?></p>
                </div>
            </div>
            <div id="div3">
                <p class="nomInfos">Description</p>
                <p class="infos"><?php echo $stockInfos['description_inter']; ?></p>
            </div>
            <div id="div4">
                <div>
                    <p class="nomInfos">Intervenant(s)</p>
                    <p class="infos"><?php echo "- " . $stockInfos['nom'] . " " . $stockInfos['prenom']; ?></p>
                </div>
            </div>
        </div>

    </div>

    <div id="troisBoutons">
        <div>
            <?php echo "<a href='EditionIntervention.php?id_intervention=" . ($stockInfos['id_intervention']) . "&id_salarie=" . ($stockInfos['id_salarie']) . "'><p>Editer</p></a>"; ?>
        </div>
        <div>
            <?php echo "<a href='?sup=ok&id_intervention=" . ($stockInfos['id_intervention']) . "&id_salarie=" . ($stockInfos['id_salarie']) . "'><p>Supprimer</p></a>"; ?>
        </div>
        <div>
            <a href="listeInterventions.php">

                <p>Retour à la liste</p>
            </a>
        </div>
    </div>

    <?php

    $supIntervention = isset($_GET["sup"]) ? $_GET["sup"] : "";

    if ($supIntervention == 'ok') {
        try {
            $req = $pdo->prepare("DELETE FROM intervention_salarie WHERE id_intervention = ? && id_salarie = ?");
            $req->execute([$recupIdIntervention, $recupIdSalarie]);
        } catch (PDOException $e) {
            echo "Erreur suppression: " . $e->getMessage();
        }

        try {
            $req = $pdo->prepare("DELETE FROM intervention WHERE id_intervention = ?");
            $req->execute([$recupIdIntervention]);
            header("Location: listeInterventions.php");
        } catch (PDOException $e) {
            echo "Erreur suppression: " . $e->getMessage();
        }
    }

    ?>

</body>

</html>