<?php

session_start();
?>


<!DOCTYPE html>

<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>ajoutEmployé</title>
</head>

<body>


    <?php
    include "config.php";
    include "navAdmin.html";
    ?>


    <h1>AJOUT D'UN EMPLOYÉ</h1>

</body>

<?php

$nom = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom'] : "";
$prenom = isset($_POST['prenom']) && !empty($_POST['prenom']) ? $_POST['prenom'] : "";
$adresse = isset($_POST['adresse']) && !empty($_POST['adresse']) ? $_POST['adresse'] : "";
$code_postal = isset($_POST['code_postal']) && !empty($_POST['code_postal']) ? $_POST['code_postal'] : "";
$ville = isset($_POST['ville']) && !empty($_POST['ville']) ? $_POST['ville'] : "";
$telephone = isset($_POST['telephone']) && !empty($_POST['telephone']) ? $_POST['telephone'] : "";
$mail = isset($_POST['mail']) && !empty($_POST['mail']) ? $_POST['mail'] : "";
$date_embauche = isset($_POST['date_embauche']) && !empty($_POST['date_embauche']) ? $_POST['date_embauche'] : "";
$identifiant = isset($_POST['identifiant']) && !empty($_POST['identifiant']) ? $_POST['identifiant'] : "";
$mdp = isset($_POST['mdp']) && !empty($_POST['mdp']) ? $_POST['mdp'] : "";
$salarie_role = isset($_POST['salarie_role']) && !empty($_POST['salarie_role']) ? $_POST['salarie_role'] : "";
$submit = isset($_POST['submit']) && !empty($_POST['submit']) ? $_POST['submit'] : '';

?>
<?php

if ($submit) {
    try {
        $request = $pdo->prepare("INSERT INTO salarie
                    (nom,prenom,adresse,code_postal,ville,telephone,mail,date_embauche,identifiant,mdp,salarie_role)
                    VALUES (:nom,:prenom,:adresse,:code_postal,:ville,:telephone,:mail,:date_embauche,:identifiant,:mdp,:salarie_role)");
        $request->execute([
            'nom' => $nom, 'prenom' => $prenom, 'adresse' => $adresse, 'code_postal' => $code_postal, 'ville' => $ville, 'telephone' => $telephone,
            'mail' => $mail, 'date_embauche' => $date_embauche, 'identifiant' => $identifiant, 'mdp' => $mdp, 'salarie_role' => $salarie_role
        ]);
        header('Location: listeEmploye.php');
    } catch (PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }
}

?>
<div id="ajout_employe">

    <form id="form" method="post">
        <div class="div1">
            <div>
                <p>Nom</p> <input type="text" name="nom" placeholder="">
            </div>
            <div>
                <p>Prenom</p><input type="text" name="prenom" placeholder="">
            </div>
        </div>
        <div class="div1">
            <div>
                <p>Adresse</p> <input type="text" name="adresse" placeholder="">
            </div>
            <div>
                <p>Code_postal</p> <input type="text" name="code_postal" placeholder="">
            </div>
            <div>
                <p>Ville</p> <input type="text" name="ville" placeholder="">
            </div>
        </div>
        <div id="div2">
            <div>
                <p>Telephone</p> <input type="text" name="telephone" placeholder="">
            </div>
            <div>
                <p>Mail</p> <input type="email" name="mail" placeholder="">
            </div>
        </div>
        <div id="div4">
            <div>
                <p>Date_embauche</p><input type="date" name="date" placeholder="">
                <p>Identifiant</p> <input type="text" name="identifiant" placeholder="">
                <p>Mdp</p> <input type="text" name="mdp" placeholder="">
                <p>Role du salarie</p> <input type="text" name="salarie_role" placeholder="">
            </div>
            <div><input class="submit" type="submit" name="submit" id="boutonAjout" value="Valider"></div>
        </div>


</html>