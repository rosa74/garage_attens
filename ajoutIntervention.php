<?php

session_start();


?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>ajout intervention</title>
</head>

<body>
    <?php
    include "config.php";
    include "nav.html";
    ?>

    <h1> Ajout d'une intervention </h1>

    <?php

    $recupIntitule = isset($_POST['intitule']) && !empty($_POST['intitule']) ? $_POST['intitule'] : "";
    $recupDate = isset($_POST['date']) && !empty($_POST['date']) ? $_POST['date'] : "";
    $recupHeure = isset($_POST['heure']) && !empty($_POST['heure']) ? $_POST['heure'] : "";
    $recupDuree = isset($_POST['duree']) && !empty($_POST['duree']) ? $_POST['duree'] : "";
    $recupClient = isset($_POST['client']) && !empty($_POST['client']) ? $_POST['client'] : "";
    $recupDescription = isset($_POST['description']) && !empty($_POST['description']) ? $_POST['description'] : "";
    $recupEmploye = isset($_POST['employe']) && !empty($_POST['employe']) ? $_POST['employe'] : "";

    ?>

    <div id="Ajout_Intervention">

        <form id="form" method="post">
            <div class="div1">
                <div>
                    <p>Intitulé</p> <input type="text" name="intitule" placeholder="">
                </div>
                <div>
                    <p>Date</p><input type="date" name="date" placeholder="">
                </div>
            </div>
            <div class="div1">
                <div>
                    <p>Heure</p> <input type="text" name="heure" placeholder="">
                </div>
                <div>
                    <p>Durée (en min)</p> <input type="number" name="duree" placeholder="">
                </div>
            </div>
            <div id="div2">
                <div>
                    <p>Client (Nom Prénom)</p> <input type="text" name="client" placeholder="">
                </div>
            </div>
            <div id="div3">
                <p>Description</p><textarea name="description" placeholder="" rows="5" cols="33"></textarea>
            </div>
            <div id="div4">
                <div>
                    <p>Intervenant(s)</p><select name="employe" multiple size=5>
                        <?php

                        try {
                            $req = $pdo->prepare("SELECT nom, id_salarie FROM salarie"); 
                            $req->execute();
                            $req = $pdo->prepare("SELECT nom, prenom, id_salarie FROM salarie"); 
                            $req->execute();" ".$salarie['prenom'].
                            $results = $req->fetchAll();
                                foreach($results as $salarie) {
                        echo "<option value='".$salarie['id_salarie']."'>".$salarie['nom']." ".$salarie['prenom']."</option>";
                    }

                        } catch (PDOException $e) {
                            echo "Erreur select : " . $e->getMessage();
                        }

                        ?>
                    </select>
                </div>
                <div><input class="submit" type="submit" name="submit" id="boutonAjout" value="Valider"></div>
            </div>
        </form>

    </div>

    <?php

    if (isset($_POST['submit'])) {
        if (
            isset($_POST['intitule']) && !empty($_POST['intitule'])
            && isset($_POST['date']) && !empty($_POST['date'])
            && isset($_POST['duree']) && !empty($_POST['duree'])
            && isset($_POST['client']) && !empty($_POST['client'])
            && isset($_POST['description']) && !empty($_POST['description'])
        ) {
            try {
                $req = $pdo->prepare("INSERT INTO intervention (intitule, date_inter, heure_inter, duree, nom_prenom_client, description_inter) VALUES (?,?,?,?,?,?)");
                $req->execute([$recupIntitule, $recupDate, $recupHeure, $recupDuree, $recupClient, $recupDescription]);
            } catch (PDOException $e) {
                echo "Erreur insert into: " . $e->getMessage();
            }

            try {
                $req = $pdo->prepare("SELECT id_intervention FROM intervention ORDER BY id_intervention DESC LIMIT 1");
                $req->execute();
                $results = $req->fetchAll();
                foreach ($results as $salarie) {
                    $recupIdIntervention = $salarie['id_intervention'];
                }
            } catch (PDOException $e) {
                echo "Erreur select : " . $e->getMessage();
            }

            try {
                $req = $pdo->prepare("INSERT INTO intervention_salarie (id_intervention, id_salarie) VALUES (?,?)");
                $req->execute([$recupIdIntervention, $recupEmploye]);
                header("Location: listeInterventions.php");
            } catch (PDOException $e) {
                echo "Erreur insert into: " . $e->getMessage();
            }
        } else {
            echo '<p id="erreur">Veuillez remplir tous les champs SVP</p>';
        }
    }

    ?>



</body>

</html>