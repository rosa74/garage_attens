<?php

session_start();

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>connexion admin</title>
</head>

<body>

    <?php

    include "config.php";
    include "nav_connexion.html";
    ?>

    <div id="formulaire">


        <form id="connexion" method="get" action="listeIntervention.php">

            <p>Identifiant</p> <input type="text" name="identifiant" placeholder=""><br>
            <p>Mot de passe</p><input type="password" name="mdp" placeholder=""> <br>
            <input class="submit" type="submit" name="submit" id="envoyer" value="Connexion">
        </form>

    </div>


    <?php

    $recupIdentifiant = isset($_POST['identifiant']) && !empty($_POST['identifiant']) ? $_POST['identifiant'] : "";
    $recupMdp = isset($_POST['mdp']) && !empty($_POST['mdp']) ? $_POST['mdp'] : "";

    if (isset($_POST['submit'])) {
        if (isset($_POST['identifiant']) && !empty($_POST['identifiant']) && isset($_POST['mdp']) && !empty($_POST['mdp'])) {
            try {
                $req = $pdo->prepare("SELECT * FROM salarie");
                $req->execute();
                $results = $req->fetchAll();
                $identifiantValid = null;
                foreach ($results as $salarie) {
                    if ($recupIdentifiant == $salarie["administrateur"] && password_verify($recupMdp, $salarie["mdp"])) {
                        $_SESSION['logged_in'] = true;
                        header("Location: listeInterventions.php");
                        $identifiantValid = true;
                    }
                }
                if ($identifiantValid != true) {
                    echo "<p id='erreur'>L'identifiant ou le mot de passe n'est pas valide</p>";
                }
            } catch (PDOException $e) {
                echo "Erreur insert into: " . $e->getMessage();
            }
        } else {
            echo "<p id='erreur'>Veuillez remplir tous les champs SVP</p>";
        }
    }

    ?>


</body>

</html>
</body>

</html>