CREATE DATABASE IF NOT EXISTS garage;
USE garage;

CREATE TABLE salarie(
  id_salarie      INT   PRIMARY KEY AUTO_INCREMENT,
  nom             VARCHAR(80)   NOT NULL, 
  prenom          VARCHAR(80)   NOT NULL, 
  adresse         VARCHAR(150)  NOT NULL, 
  code_postal     VARCHAR(5)    NOT NULL, 
  ville           VARCHAR(50)   NOT NULL, 
  telephone       VARCHAR(15)   NOT NULL, 
  mail            VARCHAR(50)   NOT NULL,
  photo           TEXT,
  date_embauche   DATE          NOT NULL,
  administrateur  VARCHAR(50)   NOT NULL    UNIQUE,
  mdp             VARCHAR(255)   NOT NULL,
  salarie_role    TINYINT       NOT NULL
);
CREATE TABLE intervention(
  id_intervention       INT PRIMARY KEY AUTO_INCREMENT,
  nom_prenom_client     VARCHAR(255)  NOT NULL,
  date_inter            DATE         NOT NULL,
  heure_inter           TIME         NOT NULL,
  intitule              VARCHAR(255) NOT NULL,
  description_inter     TEXT         NOT NULL,
  duree                 INT          NOT NULL
);
CREATE TABLE intervention_salarie(
  id_intervention       INT       NOT NULL,
  id_salarie            INT          NOT NULL,
  FOREIGN KEY (id_intervention) REFERENCES intervention(id_intervention),
  FOREIGN KEY (id_salarie) REFERENCES salarie(id_salarie)
);
