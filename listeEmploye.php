<?php

session_start();


?>


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>liste employé</title>
</head>

<body>
    <?php
    include "config.php";
    include "navAdmin.html";
    ?>

    <h2>LISTE DES EMPLOYES</h2>

    <div id="liste_employe">
        <?php

        try {
            $req = $pdo->prepare("SELECT * FROM salarie");
            $req->execute();
            $results = $req->fetchALL();

            echo '<table><tr><th>Nom</th><th>Prenom</th><th>Adresse</th><th>Code_postal</th><th>Ville</th><th>Telephone</th><th>Mail</th>
            <th>Date_embauche</th><th>Identifiant</th><th>Mdp</th><th>Salarie_role</th><th></th></tr>';

            foreach ($results as $salarie) {
                echo "  <tr>
                    <td>" . ($salarie['nom']) . "</td>
                    <td>" . ($salarie['prenom']) . "</td>
                    <td>" . ($salarie['adresse']) . "</td>
                    <td>" . ($salarie['code_postal']) . "</td>
                    <td>" . ($salarie['ville']) . "</td>
                    <td>" . ($salarie['telephone']) . "</td>
                    <td>" . ($salarie['mail']) . "</td>
                    <td>" . ($salarie['date_embauche']) . "</td>
                    <td>" . ($salarie['identifiant']) . "</td>
                    <td>" . ($salarie['mdp']) . "</td>
                    <td>" . ($salarie['salarie_role']) . "</td>
                    <td><div id='boutonVoir'>
                        <a href='InfosEmploy.php?id_salarie=" . ($salarie['id_salarie']) . "'>Voir</a>
                        </div></td>
                </tr>";
            }
            echo '</table>';
        } catch (PDOException $e) {
            echo "Erreur affichage liste: " . $e->getMessage();
        }

        ?>


    </div>
</body>

</html>