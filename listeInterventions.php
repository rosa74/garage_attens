<?php

session_start();


?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>liste intervention</title>
</head>

<body>
    <?php
    include "config.php";
    include "nav.html";
    ?>


    <h1 class="ajout">LISTE DES INTERVENTIONS </h1>


    <div id="Liste_Interventions">

                <div>
                    <a href="ajoutIntervention.php">Ajouter une intervention</a>
                </div>
    </div>

    <?php

    try {
        $req = $pdo->prepare("SELECT * FROM intervention_salarie
                         INNER JOIN salarie ON salarie.id_salarie = intervention_salarie.id_salarie
                         INNER JOIN intervention ON intervention.id_intervention = intervention_salarie.id_intervention
                         WHERE DATE(intervention.date_inter) > '2020-03-20'
                         ORDER BY intervention.date_inter DESC, intervention.heure_inter DESC
                         ");
        $req->execute();
        $results = $req->fetchALL();
        echo '<table><tr><th>Date</th><th>Heure</th><th>Intitulé</th><th>Client</th><th>Employé(s)</th><th></th></tr>';
        foreach ($results as $intervention) {
            echo "  <tr>
                    <td>" . ($intervention['date_inter']) . "</td>
                    <td>" . ($intervention['heure_inter']) . "</td>
                    <td>" . ($intervention['intitule']) . "</td>
                    <td>" . ($intervention['nom_prenom_client']) . "</td>
                    <td>" . ($intervention['nom'] . " " . $intervention['prenom']) . "</td>
                    <td><div id='boutonVoir'>
                        <a href='InfosIntervention.php?id_intervention=" . ($intervention['id_intervention']) . "&id_salarie=" . ($intervention['id_salarie']) . "'>Voir</a>
                        </div></td>
                </tr>";
        }
        echo '</table>';
    } catch (PDOException $e) {
        echo "Erreur affichage liste: " . $e->getMessage();
    }

    ?>


    </div>
</body>

</html>